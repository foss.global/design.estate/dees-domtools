import { expect, tap } from '@pushrocks/tapbundle';
import * as domtools from '../ts/index';

tap.test('first test', async () => {
  const domtoolsInstance = await domtools.DomTools.setupDomTools();
  expect(domtoolsInstance).toBeInstanceOf(domtools.DomTools);
});

tap.start();
