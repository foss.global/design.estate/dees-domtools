// designestate scope
import * as deesComms from '@designestate/dees-comms';

export { deesComms };

// apiglobal scope
import * as typedrequest from '@apiglobal/typedrequest';

export { typedrequest };

// pushrocks scope
import * as smartdelay from '@pushrocks/smartdelay';
import * as smartjson from '@pushrocks/smartjson';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartrouter from '@pushrocks/smartrouter';
import * as smartrx from '@pushrocks/smartrx';
import * as smartstate from '@pushrocks/smartstate';
import * as smarturl from '@pushrocks/smarturl';
import * as webrequest from '@pushrocks/webrequest';
import * as websetup from '@pushrocks/websetup';
import * as webstore from '@pushrocks/webstore';

// subscope lik
import { ObjectMap } from '@pushrocks/lik/dist_ts/lik.objectmap.js';
import { Stringmap } from '@pushrocks/lik/dist_ts/lik.stringmap.js';
import { FastMap } from '@pushrocks/lik/dist_ts/lik.fastmap.js';
const lik = {
  ObjectMap,
  Stringmap,
  FastMap,
};

export {
  lik,
  smartdelay,
  smartjson,
  smartpromise,
  smartrouter,
  smartrx,
  smarturl,
  smartstate,
  webrequest,
  websetup,
  webstore,
};

// third party scope
import SweetScroll from 'sweet-scroll';

export { SweetScroll };
