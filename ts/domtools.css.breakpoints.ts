import { DomTools } from './domtools.classes.domtools.js';

import { CSSResult, unsafeCSS } from 'lit';

export const desktop = 1600;
export const notebook = 1240;
export const tablet = 1024;
export const phablet = 600;
export const phone = 400;

export type TViewport = 'native' | 'desktop' | 'tablet' | 'phablet' | 'phone';

export const cssForDesktop = (cssArg: CSSResult) => {
  return unsafeCSS(`
    @container wccToolsViewport (min-width: ${desktop}px) {
      ${cssArg.cssText}
    }
    @media (min-width: ${desktop}px) {
      ${cssArg.cssText}
    }
  `);
};

export const cssForNotebook = (cssArg: CSSResult) => {
  return unsafeCSS(`
    @container wccToolsViewport (max-width: ${notebook}px) {
      ${cssArg.cssText}
    }
    @media (max-width: ${notebook}px) {
      ${cssArg.cssText}
    }
  `);
};

export const cssForTablet = (cssArg: CSSResult) => {
  return unsafeCSS(`
    @container wccToolsViewport (max-width: ${tablet}px) {
      ${cssArg.cssText}
    }
    @media (max-width: ${tablet}px) {
      ${cssArg.cssText}
    }
  `);
};

export const cssForPhablet = (cssArg: CSSResult) => {
  return unsafeCSS(`
    @container wccToolsViewport (max-width: ${phablet}px) {
      ${cssArg.cssText}
    }
    @media (max-width: ${phablet}px) {
      ${cssArg.cssText}
    }
  `);
};

export const cssForPhone = (cssArg: CSSResult) => {
  return unsafeCSS(`
    @container wccToolsViewport (max-width: ${phone}px) {
      ${cssArg.cssText}
    }
    @media (max-width: ${phone}px) {
      ${cssArg.cssText}
    }
  `);
};
