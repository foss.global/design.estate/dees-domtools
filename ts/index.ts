export * from './domtools.colors.js';

import * as elementBasic from './domtools.elementbasic.js';
import * as breakpoints from './domtools.css.breakpoints.js';
import * as css from './domtools.css.js';

export { css, breakpoints, elementBasic };
export { DomTools, IDomToolsContructorOptions } from './domtools.classes.domtools.js';
export { TypedRequest } from '@apiglobal/typedrequest';
export { IWebSetupConstructorOptions } from '@pushrocks/websetup';
export { rxjs } from '@pushrocks/smartrx';

import * as allPlugins from './domtools.plugins.js';

export const plugins = {
  smartdelay: allPlugins.smartdelay,
  smartpromise: allPlugins.smartpromise,
  SweetScroll: allPlugins.SweetScroll,
  smartstate: allPlugins.smartstate,
};
