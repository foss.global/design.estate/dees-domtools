export interface IDeesColorSet {
  primaryAccent: string;
  secondaryAccent: string;
  primaryBackground: string;
  secondaryBackground: string;
}
