export const cssGridColumns = (amountOfColumnsArg: number, gapSizeArg: number) => {
  let returnString = ``;
  for (let i = 0; i < amountOfColumnsArg; i++) {
    returnString += ` calc((100%/${amountOfColumnsArg}) - (${
      gapSizeArg * (amountOfColumnsArg - 1)
    }px/${amountOfColumnsArg}))`;
  }
  return returnString;
};
