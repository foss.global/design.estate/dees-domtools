import { DomTools } from './domtools.classes.domtools.js';

import { css, unsafeCSS } from 'lit';

/**
 * changes scrollbar styles to be consistent across OS borders
 */
export const scrollBarStyles: string = (() => {
  const returnStylesOld = navigator.userAgent.indexOf('Mac OS X') === -1 ? css``.cssText : ``;
  const returnStyles = css`
    /* width */
    ::-webkit-scrollbar {
      width: 0px;
    }
    body {
      scrollbar-width: none;
    }
  `.cssText;
  return returnStyles;
})();

export const globalBaseStyles: string = css`
  /*
  @font-face {
    font-family: 'Material Icons';
    font-style: normal;
    font-weight: 400;
    src: url(https://fonts.gstatic.com/s/materialicons/v42/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2)
      format('woff2');
  } */

  /* global body styles */
  body {
    margin: 0px;
    font-family: 'Mona Sans', 'Inter', sans-serif;
    box-sizing: border-box;
  }

  /* scroll bar styles */
  ${unsafeCSS(scrollBarStyles)}
`.cssText;
