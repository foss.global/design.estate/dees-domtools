/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@design.estate/dees-domtools',
  version: '2.0.37',
  description: 'tools to simplify complex css structures'
}
