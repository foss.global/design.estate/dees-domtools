import * as plugins from './domtools.plugins.js';
import { DomTools, IDomToolsContructorOptions } from './domtools.classes.domtools.js';
import { scrollBarStyles, globalBaseStyles } from './domtools.css.basestyles.js';

import { html, LitElement, css, unsafeCSS } from 'lit';

export const staticStyles = css`
  * {
    transition: background 0.1s, color 0.1s;
    box-sizing: border-box;
  }

  :host {
    box-sizing: border-box;
    font-family: 'Mona Sans', 'Inter', sans-serif;
  }

  ${unsafeCSS(scrollBarStyles)}
`;

/**
 * styles to be included in every webcomponent
 */
export const styles = html`
  <style>
    * {
      transition: background 0.1s, color 0.1s;
      box-sizing: border-box;
    }

    :host {
      font-family: 'Mona Sans', 'Inter', sans-serif;
    }

    ${scrollBarStyles}
  </style>
`;

/**
 * a basic setup for elements
 * makes sure everything is in check
 */
export const setup = async (
  elementArg?: LitElement,
  optionsArg: IDomToolsContructorOptions = {}
): Promise<DomTools> => {
  const domTools = await DomTools.setupDomTools(optionsArg);

  if (elementArg) {
    // lets do something with the element
    // not used right now
  }

  domTools.runOnce('elementBasicSetup', async () => {
    // bodyStyles
    domTools.setGlobalStyles(globalBaseStyles);
  });
  return domTools;
};
